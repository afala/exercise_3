/*
 * packet.h

 *
 *  Created on: Jun 10, 2018
 *      Author: compm
 */
#include "logger.h"
#include <fcntl.h>

#define SUCCESS 0
#define FAIL -1

#define OPCODE_LEN 2
#define BLOCK_NUM_LEN 2
#define FULL_DATA_LEN 516
#define DATA_LEN 512
#define DATA_OFFSET 4

#define OPCODE_WRQ 2
#define OPCODE_DATA 3
#define OPCODE_ACK 4

#define BAD_OPCODE -1
#define BAD_ALLOC -2
#define BAD_OPEN -3
#define BAD_PACKET -4
#define BAD_WRITE -5

#define REG_PACKET -6
#define LAST_PACKET -7



struct ackMsg{
	uint16_t opcode;
	uint16_t blockNum;
} __attribute__((packed));

int checkOpcode (char* buffer, int wanted_op)
int recvWRQ (char* buffer);
int recvData (int fd, char* buffer, int ack_num, int msg_size);
int sendAck(int fd, struct sockaddr_in* client_addr, int client_addr_len, int ack_num);
