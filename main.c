//
// Created by Roy on 09/06/2018.
//
#include "logger.h"
#include "packet.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


#define TRANSMISSION_SIZE 516


const int WAIT_FOR_PACKET_TIMEOUT = 3;
const int NUMBER_OF_FAILURES = 7;

int timeoutExpiredCount = 0;
int lastWriteSize = 0;
uint16_t ackCount = 0;

struct wrqBlock{
	uint16_t opcode;
	char* fileName;
	char st1;
	char* transmissionMode;
	char st2;
} __attribute__((packed));


struct dataBlock{
	uint16_t opcode;
	uint16_t blockNumber;
} __attribute__((packed));

int main(int argc, char **argv){

	if(argc != 2){ //gets as parameter only port number
		printf("Bad Parameters");
		return FAIL;
	}

	timeoutExpiredCount = 0;
	int port = atoi(argv[1]);

	/* socket() - Create socket for sending/receiving datagrams */
	int sock =  socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0){
		sysCallErrorAux();
		return FAIL;
	}

	/* bind() - Attach socket to the given port*/
	struct sockaddr_in my_addr;
	memset(&my_addr, 0, sizeof(struct sockaddr_in)); //clear struct
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(port); //host to network short
	my_addr.sin_addr = htons(INADDR_ANY);
	int bind_ret = bind(sock,(struct sockaddr*)&my_addr,sizeof(my_addr));
	if(bind_ret < 0){
		sysCallErrorAux();
		return FAIL;
	}




	while(true){

	// recvfrom() - Get size of first message from client (supposed to be WRQ)
	struct sockaddr_in client_addr;
	char buffer[TRANSMISSION_SIZE] = {0};
	int client_addr_size = sizeof(client_addr);
	int msgLength = recvfrom(sock, buffer, TRANSMISSION_SIZE, 0, (struct sockaddr*)&client_addr, &client_addr_size);
	if (msgLength < 0){
		sysCallErrorAux();
		return FAIL;
	}

	// check for WRQ packet
	int fd = recvWRQ(buffer)<0;
	if(fd<0){
		if(fd == BAD_OPCODE)
			flowErrorAux("Did not receive an expected WRQ packet");
		recFailAux();
		continue;
	}

	//here only if received a WRQ packet. now we need to send ACK to the client
	if(sendAck(sock,&client_addr,client_addr_size,ackCount) == FAIL){
		flowErrorAux("Got WRQ packet, could not send back ACK packet");
		recFailAux();
		continue;
	}
	ackCount++; //get the Ack block number ready for next time

	//set the timeval struct for timeout
	struct timeval tv;
	tv.tv_sec = WAIT_FOR_PACKET_TIMEOUT;
	tv.tv_usec = 0;
	//connect read pointer to socket for the select call
	fd_set readfds;
	FD_ZERO(&readfds); //clear bit set
	FD_SET(sock, &readfds);
	bool data_flow = true;

	do
	{
		do
		{
			do
			{
				int sel = select(sock+1,&readfds,NULL,NULL,&tv); // Wait WAIT_FOR_PACKET_TIMEOUT to see if something appears for us at the socket (we are waiting for DATA)
				if (sel > 0)// TODO: if there was something at the socket and we are here not because of a timeout
				{
					int sockMsg = recvfrom(sock,buffer,FULL_DATA_LEN,0,(struct sockaddr *)&client_addr,&client_addr_size); // Read the DATA packet from the socket (at least we hope this is a DATA packet)
					if(sockMsg < 0){
						sysCallErrorAux();
						close(sock);
						return FAIL; // FATAL ERROR BAIL OUT
					}
					int packet_type = recvData(fd,buffer,ackCount,msgLength);
					if (packet_type == REG_PACKET){
						if(sendAck(sock,&client_addr,client_addr_size,ackCount) == FAIL){
							flowErrorAux("Got DATA packet, could not send back ACK packet");
							recFailAux();
							close(sock);
							return FAIL;
						}
						ackCount++;
						continue;
					}
					else if (packet_type == LAST_PACKET)
						data_flow = false;
					else // recvfrom failed to read data
						continue;
				}
				else if (sel == 0) //Time out expired while waiting for data to appear at the socket
				{
				if(sendAck(sock,&client_addr,client_addr_size,ackCount) == FAIL){ //Send another ACK for the last packet
					flowErrorAux("could not send ACK for the last packet");
					recFailAux();
					return FAIL;
					}
				ackCount++;
				timeoutExpiredCount++;
				}
				if (timeoutExpiredCount>= NUMBER_OF_FAILURES)
				{
					flowErrorAux("Number of failures passed the threshold. Existing Program...");
					recFailAux();
					close(socket);
					return FAIL; // FATAL ERROR BAIL OUT
				}
			}while (data_flow); // Continue while some socket was ready but recvfrom somehow failed to read the data
		if (1) // TODO: We got something else but DATA
		{
		// FATAL ERROR BAIL OUT
		}
		if (1) // TODO: The incoming block number is not what we have expected, i.e. this is a DATA pkt but the block number in DATA was wrong (not last ACK’s block number + 1)
		{
		// FATAL ERROR BAIL OUT
		}
		}while (false);
	timeoutExpiredCount = 0;
	lastWriteSize = fwrite(1); // write next bulk of data
	// TODO: send ACK packet to the client
	}while (1); // Have blocks left to be read from client (not end of transmission)

	}

	int finish = close(sock);
	if(finish < 0)
		sysCallErrorAux();
	return finish;
} //main
