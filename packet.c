/*
 * packet.c
 *
 *  Created on: Jun 10, 2018
 *      Author: compm
 */
#include "packet.h"




int checkOpcode (char* buffer, int wanted_op){
	uint16_t opcode = 0;
	memcpy(&opcode, buffer, OPCODE_LEN);
	if (ntohs(opcode) != wanted_op)
	{
		flowErrorAux("Wrong Opcode");
		return FAIL;
	}
	return SUCCESS;
}

int recvWRQ (char* buffer){
	//check if opcode is a correct WRQ opcode
	if(checkOpcode(buffer, OPCODE_WRQ) == FAIL)
		return BAD_OPCODE;
	//get file name from WRQ packet
	char* fileName;
	fileName = strdup(buffer+OPCODE_LEN);
	if (fileName == NULL)
	{
		sysCallErrorAux();
		return BAD_ALLOC;
	}
	//Open File
	int fd = open(fileName, O_RDWR);
	if(fd < 0){
		free(fileName);
		sysCallErrorAux();
		return BAD_OPEN;
	}
	//get transmission mode from WRQ packet
	char* mode;
	mode = strdup(buffer+OPCODE_LEN+strlen(fileName)+1);
	if (mode == NULL)
	{
		free(fileName);
		sysCallErrorAux();
		return BAD_ALLOC;
	}
	//successfully got the WRQ packet
	recWrqPacketAux(fileName,mode);
	free(fileName);
	free(mode);
	return fd;
}


int recvData (int fd, char* buffer, uint16_t ack_num, int msg_size){
	//check if opcode is a correct DATA opcode
	if(checkOpcode(buffer, OPCODE_DATA) == FAIL)
		return BAD_OPCODE;
	uint16_t blockNum=0;
	memcpy(&blockNum,buffer+OPCODE_LEN,BLOCK_NUM_LEN);
	if(blockNum != ackCount)
		return BAD_PACKET;
	uint16_t blockNum_fix = ntohs(blockNum);
	recDataPacketAux(blockNum_fix,msg_size);
	//packet is ok. lets get the data
	char* data = (char*)malloc(DATA_LEN); //size of char is 1
	if(data==NULL)
		return BAD_ALLOC;
	int data_size = msg_size - DATA_OFFSET;
	memcpy(data, buffer + DATA_OFFSET, data_size);
	//write data to fd
	writeBlockAux(data_size);
	if (write(fd, data, data_size) < data_size){
		sysCallErrorAux();
		free(data);
		return BAD_WRITE;
	}
	free(data);
	if(data_size < DATA_LEN)
		return LAST_PACKET;
	return REG_PACKET;

}


int sendAck(int fd, struct sockaddr_in* client_addr, int client_addr_len, uint16_t ack_num){
	struct ackMsg ack;
	ack.opcode = htons(OPCODE_ACK);
	ack.blockNum = htons(ack_num);
	if (sendto(fd,(void*)(&ack),sizeof(ack),0,(struct sockaddr*)client_addr,client_addr_len) != sizeof(ack))
	{
		sysCallErrorAux();
		return FAIL;
	}
	sendAckPacketAux(ack_num);
	return SUCCESS;
}

